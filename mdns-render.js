'use strict';

var electron = require('electron');
var ipc = electron.ipcRenderer;

ipc.on('deviceFound', function (e, packet) {
  var name = packet.answer[0].data.replace('.' + packet.answer[0].name, '');
  var ips = [];

  packet.additional.forEach(function(record) {
    if (record.name.indexOf(name) !== 0) {
      console.warn('unexpected record name ' + record.name, record);
      return;
    }

    if (record.typeName === 'A') {
      ips.push({ family: 'ipv4', address: record.address });
    }
    else if (record.typeName === 'AAAA') {
      ips.push({ family: 'ipv6', address: record.address });
    }
    else {
      console.log('mDNS record not handled', record);
    }
  }, this);

  ips.sort(function (addrA, addrB) {
    if (addrA.family < addrB.family) {
      return -1;
    } else if (addrA.family > addrB.family) {
      return 1;
    }

    else if (addrA.address < addrB.address) {
      return -1;
    } else if (addrA.address > addrB.address) {
      return 1;
    }
    return 0;
  });

  var container = document.querySelector('.found-devices');
  Array.prototype.slice.call(container.children).forEach(function (elem) {
    if (elem.classList.contains(name)) {
      container.removeChild(elem);
    }
  });

  var elem = document.createElement('div');
  elem.classList.add('device');
  elem.classList.add(name);

  var header = document.createElement('h4');
  header.textContent = name;
  elem.appendChild(header);

  var list = document.createElement('ul');
  ips.forEach(function (addr) {
    var subElem = document.createElement('li');
    subElem.textContent = addr.address;
    list.appendChild(subElem);
  });
  elem.appendChild(list);

  container.appendChild(elem);
});

document.body.addEventListener('click', function (ev) {
  if (ev.target.classList.contains('query-devices')) {
    ipc.send('startDeviceScan');
  }
});
