'use strict';

var electron = require('electron');
var app = electron.app;
var ipc = electron.ipcMain;

var win;

function increaseBadge() {
  if (!win.isFocused()) {
    app.setBadgeCount(app.getBadgeCount() + 1);
  }
}
function clearBadge() {
  app.setBadgeCount(0);
}

function updateProgress(count, target) {
  if (count < 0 || count <= target) {
    win.setProgressBar(0);
  } else {
    win.setProgressBar(count/target);
  }
}

function init(window) {
  if (win) {
    console.error("can't initialize badge/progress tracker multiple times");
    return;
  }
  win = window;

  ipc.on('notification', function (ev, count, target) {
    updateProgress(count, target);
    increaseBadge();
  });
  win.on('focus', clearBadge);
}

module.exports.init = init;
