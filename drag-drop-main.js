'use strict';

var electron = require('electron');
var app = electron.app;
var ipc = electron.ipcMain;

var win;

function sliceArgv(argv) {
  var count = 1;
  // We need to determine if we were run using electron or as a system installed app.
  if (argv[0].search('electron') >= 0) {
    count += 1;
  }
  return argv.slice(count);
}
var argv = sliceArgv(process.argv);

function processArgv(argv) {
  var files = [];
  argv.forEach(function(arg) {
    if (arg.substr(0, 2) === '--') {
      console.log('received argument', arg);
    } else {
      files.push(arg);
    }
  });

  if (files.length > 0) {
    win.webContents.send('files', files);
  }
}

function onOpen(e, filename) {
  e.preventDefault();

  if (app.ipcReady) {
    processArgv([ filename ]);
  } else {
    argv.push(filename);
  }
}
app.on('open-file', onOpen);
app.on('open-url', onOpen);

function init(window) {
  if (win) {
    console.error("can't initiliaze drag-and-drop multiple times");
    return;
  }
  win = window;

  ipc.once('ipcReady', function () {
    app.ipcReady = true;
    processArgv(argv);
  });
}

module.exports.init = init;
