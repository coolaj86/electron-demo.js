'use strict';

var electron = require('electron');

// This file should be included from the main thread and every renderer thread.
// Note that the reporter is only half of the equation. A server listening for
// the reports is also required.
//
// The config is currently set up to report to a locally running instance of
// https://github.com/electron/mini-breakpad-server.
// Setup instructions on that page are fairly simple, though I don't know what the
// breakpad symbols parts means and I didn't use it to initially test this feature.
// If you want to force a crash for testing call `process.crash`

electron.crashReporter.start({
  companyName: 'Daplie Labs',
  productName: 'Electron Demo',
  submitURL: 'http://localhost:1127/post',
  extra: {
    prop: 'some string value',
  },
});

// If we are in a render thread add a hook catch clicks on "crasher" buttons
// that will crash the program
if (typeof document !== 'undefined') {
  document.body.addEventListener('click', function (ev) {
    if (ev.target.classList.contains('crasher')) {
      process.crash();
    }
  });
}
