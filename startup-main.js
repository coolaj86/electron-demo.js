'use strict';

var AutoLaunch = require('auto-launch');
var electron = require('electron');
var app = electron.app;
var ipc = electron.ipcMain;

// On Mac, work around a bug in auto-launch where it opens a Terminal window
// See https://github.com/Teamwork/node-auto-launch/issues/28#issuecomment-222194437
var appPath;
if (process.platform === 'darwin') {
  appPath = app.getPath('exe').replace(/\.app\/Content.*/, '.app');
} else {
  appPath = undefined; // Use the default
}

var appLauncher = new AutoLaunch({
  name: 'ElectronDemo',
  path: appPath,
  isHidden: true
});

function install () {
  return appLauncher
    .isEnabled()
    .then(function (enabled) {
      if (!enabled) {
        return appLauncher.enable();
      }
    });
}

function uninstall () {
  return appLauncher
    .isEnabled()
    .then(function (enabled) {
      if (enabled) {
        return appLauncher.disable();
      }
    });
}

var win;
function init(window) {
  if (win) {
    console.error("can't initiliaze startup multiple times");
    return;
  }
  win = window;

  ipc.on('startupChange', function (ev, state) {
    if (state) {
      install();
    } else {
      uninstall();
    }
  });
  ipc.on('reqStartupState', function () {
    appLauncher
      .isEnabled()
      .then(function (enabled) {
        win.webContents.send('startupState', enabled);
      });
  });
}

module.exports = {
  init: init,
};
