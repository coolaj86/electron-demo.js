'use strict';

var path = require('path');
var electron = require('electron');
var app = electron.app;

var win, tray;

function updateTrayMenu () {
  if (!tray) {
    return;
  }

  var menuTemplate = [];

  if (win.isVisible()) {
    menuTemplate.push({
      label: 'Hide to tray',
      click: function () { win.hide(); },
    });
  } else {
    menuTemplate.push({
      label: 'Electron Demo App',
      click: function () { win.show(); },
    });
  }

  menuTemplate.push({
    label: 'Quit',
    click: function () { app.quit(); },
  });
  tray.setContextMenu(electron.Menu.buildFromTemplate(menuTemplate));
}

// Check to see if the tray is supported on this system. This code was mostly copied from
// the Webtorrent project.
function checkLinuxTraySupport (cb) {
  var cp = require('child_process');

  cp.exec('dpkg --get-selections libappindicator1', function (err, stdout) {
    // Unfortunately there's no cleaner way, as far as I can tell, to check
    // whether a debian package is installed:
    if (err) {
      cb(err);
    } else if (stdout.endsWith('\tinstall\n')) {
      cb(null);
    } else {
      cb(new Error('debian package not installed'));
    }
  });
}

function createTray() {
  tray = new electron.Tray(path.join(__dirname, 'images', 'daplie-logo.png'));
  tray.on('click', function () { win.show(); });
  updateTrayMenu();
}


function initLinux () {
  checkLinuxTraySupport(function (err) {
    if (!err) {
      createTray();
    }
  });
}
function initWin32 () {
  createTray();
}

function init(window) {
  if (win) {
    console.error("can't initialize the tray mulitple times");
    return;
  }

  win = window;
  win.on('blur', updateTrayMenu);
  win.on('focus', updateTrayMenu);
  win.on('hide', updateTrayMenu);
  win.on('show', updateTrayMenu);

  // Mac apps generally do not have menu bar icons
  if (process.platform === 'linux') {
    initLinux();
  }
  if (process.platform === 'win32') {
    initWin32();
  }
}

function hasTray() {
  return !!tray;
}

module.exports.init = init;
module.exports.hasTray = hasTray;
