'use strict';

var electron = require('electron');
var app = electron.app;
var BrowserWindow = electron.BrowserWindow;
var path = require('path');
var url = require('url');

require('./crash-reporter');

// Start the app without showing the main window when auto launching on login
// (On Windows and Linux, we get a flag. On MacOS, we get special API.)
var hidden = process.argv.includes('--hidden') ||
  (process.platform === 'darwin' && app.getLoginItemSettings().wasOpenedAsHidden);

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
var win;

function createWindow () {
  // Create the browser window.
  win = new BrowserWindow({
    title: 'Electron Demo',
    icon:  path.join(__dirname, 'images', 'daplie-logo.png'),
    show: !hidden,
    width:  800,
    height: 600,
    minWidth:  280,
    minHeight: 350,
  });

  var tray = require('./tray');
  tray.init(win);

  require('./menu').init(win);
  require('./progress').init(win);
  require('./drag-drop-main').init(win);
  require('./startup-main').init(win);
  require('./mdns-main').init(win);

  // // Open the DevTools.
  // win.webContents.openDevTools();

  win.webContents.on('will-navigate', function(e) {
    // Prevent drag-and-drop from navigating the Electron window, which can happen
    // before our drag-and-drop handlers have been initialized.
    e.preventDefault();
  });

  // Emitted when the window is closed.
  win.on('close', function (e) {
    if (process.platform !== 'darwin') {
      if (!tray.hasTray()) {
        app.quit();
        return;
      }
    }
    // We are either on Mac with a dock, or a system where we want to minimize to the tray, so
    // don't actually close, just hide the window.
    if (!app.isQuitting) {
      e.preventDefault();
      win.hide();
    }
  });

  // and load the index.html of the app.
  win.loadURL(url.format({
    pathname: path.join(__dirname, 'index.html'),
    protocol: 'file:',
    slashes: true
  }));
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', createWindow);

// Mark the application as quitting so the logic we have to minimize to the tray on window
// close doesn't prevent the proper exitting of the application.
app.on('before-quit', function () {
  app.isQuitting = true;
});

app.on('activate', function () {
  // On macOS it's common to re-create a window in the app when the
  // dock icon is clicked and there are no other windows open.
  if (win) {
    win.show();
  }
});

// In this file you can include the rest of your app's specific main process
// code. You can also put them in separate files and require them here.
