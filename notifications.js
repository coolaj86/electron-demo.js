'use strict';

var electron = require('electron');
var ipc = electron.ipcRenderer;
// This file runs in a render thread of the Electron app. It must be required (directly or
// indirectly) from one of the html files.

var count = 0;
var target = 0;
function notifyUser() {
  ipc.send('notification', count, target);
  count += 1;
  var notif = new window.Notification('Annoying Alert ' + count, {
    body: 'See what happens when you try to click on it.',
    silent: true,
  });

  // On my system notifications are unclickable and seem to be unclosable as well,
  // so not all of this code is fully tested.
  notif.onclick = function () {
    console.log('notification ' + count + ' clicked');
    notif.close();
  };
  notif.onclose = function () {
    if (count < target) {
      notifyUser();
    }
    else {
      count = 0;
      target = 0;
      ipc.send('notification', count, target);
    }
  };
}

function queueNotification() {
  target += 1;
  if (count === 0) {
    notifyUser();
  } else {
    ipc.send('notification', count-1, target);
  }
}

function clearQueue() {
  target = count;
  if (count > 0) {
    ipc.send('notification', count-1, target);
  }
}

document.body.addEventListener('click', function (ev) {
  if (ev.target.matches('.notify .queue')) {
    queueNotification();
  }
  else if (ev.target.matches('.notify .clear')) {
    clearQueue();
  }
});
