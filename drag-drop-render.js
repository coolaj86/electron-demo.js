'use strict';

var electron = require('electron');
var ipc = electron.ipcRenderer;

function handleFiles(files) {
  if (!Array.isArray(files)) {
    files = [ files ];
  }

  var container = document.getElementsByClassName('file-container')[0];
  container.innerHTML = '';
  files.forEach(function (name) {
    var div = document.createElement('div');
    div.innerHTML = name;
    container.appendChild(div);
  });
}

// Need to prevent default for some of the document drag-and-drop events to be able
// to properly get events. This doesn't explain why I need to preventDefault on all
// the events (or whichever combination is actually needed - haven't really spent
// time trying different combinations), but it is what put me down this path.
// https://discuss.atom.io/t/possible-to-get-local-filesystem-path-from-drag-and-drop-file/28858
function preventDefault(ev) {
  ev.preventDefault();
}
var events = ['drag','dragend','dragenter','dragexit','dragleave','dragover','dragstart','drop'];
events.forEach(function (event) {
  document.addEventListener(event, preventDefault);
});

document.body.addEventListener('drop', function (ev) {
  var area = document.body.getElementsByClassName('drag-n-drop')[0];
  if (!area.contains(ev.target)) {
    return;
  }

  var files = Array.prototype.map.call(ev.dataTransfer.files, function (file) {
    return file.path;
  });

  if (files.length > 0) {
    handleFiles(files);
  }
});

ipc.on('files', function (e, files) {
  handleFiles(files);
});
ipc.send('ipcReady');
