'use strict';

var electron = require('electron');
var ipc = electron.ipcRenderer;

ipc.on('startupState', function (e, state) {
  var checkbox = document.getElementsByClassName('auto-start')[0];
  checkbox.checked = state;
});

setTimeout(function () {
  ipc.send('reqStartupState');
}, 1000);

document.body.addEventListener('change', function (ev) {
  if (ev.target.classList.contains('auto-start')) {
    ipc.send('startupChange', ev.target.checked);
  }
});
