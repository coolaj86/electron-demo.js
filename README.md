# Electron Demo
Example of a simple `Electon` app that uses the features
[described as "sexy" by the webtorrent team](https://blog.dcpos.ch/how-to-make-your-electron-app-sexy).

## Contents
So far the following have been implemented
* `tray.js`: Tray integration
* `progress.js`: Dock badge count and window progress bar
* `notifications.js`: Notifications
* `menu.js`: Custom menu and keyboard shortcuts
* `crash-reporter.js`: Reporting side of the crash reporting
* `drag-drop-main.js`: Handling of drag-and-drop events onto the dock or launcher
* `drag-drop-render.js`: Handling of drag-and-drop events onto the window
* `startup-main.js`: Enabling and disabling auto-startup feature

**Disclaimer**: This repository has been structured to separate each feature as much
as possible. As such not all of the communication is set up in the same way it should
be in more complete and complicated applications.

## Missing
The following have not yet been implemented in this demo
* One-step build
* Signed installers for all platforms
* Automatic updaters for Mac and Windows

## Running the app without installation
```bash
npm install
npm start
```

## Creating the .deb
```bash
npm install
npm run build
npm run deb64
```

The installer will be placed in `dist/installers`
