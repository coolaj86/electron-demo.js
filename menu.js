'use strict';

var electron = require('electron');

var win, menu;

function getMenuItem (label) {
  var menuItem;
  menu.items.some(function (topLvl) {
    menuItem = topLvl.submenu.items.find(function (item) {
      return item.label === label;
    });
    return menuItem;
  });
  return menuItem || {};
}

// Sets whether the window should always show on top of other windows
function toggleAlwaysOnTop(flag) {
  if (!win)  {
    return;
  }
  if (typeof flag !== 'boolean') {
    flag = !win.isAlwaysOnTop();
  }

  win.setAlwaysOnTop(flag);
  getMenuItem('Float on Top').checked = flag;
}

function toggleDevTools() {
  if (!win)  {
    return;
  }
  if (win.webContents.isDevToolsOpened()) {
    win.webContents.closeDevTools();
  } else {
    win.webContents.openDevTools();
  }
}

function init(window) {
  if (win) {
    console.error("can't initialize the tray mulitple times");
    return;
  }
  win = window;

  var template = [
    {
      label: 'Edit',
      submenu: [
        { role: 'undo' },
        { role: 'redo' },
        { type: 'separator' },

        { role: 'cut' },
        { role: 'copy' },
        { role: 'paste' },
        { role: 'pasteandmatchstyle' },
        { role: 'delete' },
        { role: 'selectall' }
      ]
    },
    {
      label: 'View',
      submenu: [
        { role: 'reload' },
        { role: 'forcereload' },
        { type: 'separator' },

        { role: 'resetzoom' },
        { role: 'zoomin' },
        { role: 'zoomout' },
        { type: 'separator' },

        { role: 'togglefullscreen' },
        {
          label: 'Float on Top',
          type: 'checkbox',
          click: toggleAlwaysOnTop,
        },
        { type: 'separator' },

        {
          label: 'Developer',
          submenu: [
            {
              label: 'Developer Tools',
              accelerator: process.platform === 'darwin' ? 'Alt+Command+I' : 'Ctrl+Shift+I',
              click: toggleDevTools,
            },
          ]
        }
      ]
    },
  ];

  if (process.platform === 'darwin') {
    // WebTorrent menu (Mac)
    template.unshift({
      label: 'Electron Demo',
      submenu: [
        { role: 'about' },
        { type: 'separator' },

        {
          label: 'Preferences',
          accelerator: 'Cmd+,',
          click: function () { console.log('TODO: implement preferences'); }
        },
        { type: 'separator' },

        { role: 'services',   submenu: [] },
        { type: 'separator' },
        { role: 'hide' },
        { role: 'hideothers' },
        { role: 'unhide' },
        { type: 'separator' },

        { role: 'quit' }
      ]
    });

    // Edit menu (Mac)
    template[2].submenu.push(
      { type: 'separator' },
      {
        label: 'Speech',
        submenu: [
          { role: 'startspeaking' },
          { role: 'stopspeaking' }
        ]
      }
    );

    // Window menu (Mac)
    template.push({
      role: 'Window',
      submenu: [
        { role: 'close' },
        { role: 'minimize' },
        { role: 'zoom' },
        { type: 'separator' },

        { role: 'front' }
      ]
    });
  }

  // On Windows and Linux, open dialogs do not support selecting both files and
  // folders and files, so add an extra menu item so there is one for each type.
  if (process.platform === 'linux' || process.platform === 'win32') {
    template.unshift({
      label: 'File',
      submenu: [
        { role: 'quit' },
      ]
    });

    // Edit menu (Windows, Linux)
    template[1].submenu.push(
      { type: 'separator' },
      {
        label: 'Preferences',
        accelerator: 'CmdOrCtrl+,',
        click: function () { console.log('TODO: implement preferences'); }
      }
    );

    // Window menu (Windows, Linux)
    template.push({
      role: 'Qindow',
      submenu: [
        { role: 'minimize' },
        { role: 'close' },
      ]
    });

    // Add an help.about option
    template.push({
      label: 'Help',
      submenu: [
        {
          label: 'About',
          click: function () { console.log('TODO: implement about'); },
        }
      ],
    });
  }

  menu = electron.Menu.buildFromTemplate(template);
  electron.Menu.setApplicationMenu(menu);
}

module.exports.init = init;
